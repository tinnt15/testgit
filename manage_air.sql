USE [AIRPORT_MANAGE]
GO
/****** Object:  Table [dbo].[Airplane]    Script Date: 19/12/2017 2:01:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Airplane](
	[IdMb] [char](7) NOT NULL,
	[Model] [varchar](40) NULL,
	[CruiseSpeed] [varchar](30) NULL,
	[PlaneType] [varchar](30) NULL,
	[EmptyWeight] [varchar](30) NULL,
	[MaxTakeoffWeight] [varchar](30) NULL,
	[MinNeededRunwaySize] [varchar](30) NULL,
	[IDSB] [char](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdMb] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Airport]    Script Date: 19/12/2017 2:01:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Airport](
	[IdSb] [char](7) NOT NULL,
	[Name] [varchar](30) NULL,
	[Runwaysize] [varchar](30) NULL,
	[WingParKingPlace] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdSb] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Helicopter]    Script Date: 19/12/2017 2:01:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Helicopter](
	[IdTt] [char](7) NOT NULL,
	[Model] [varchar](40) NULL,
	[CruiseSpeed] [varchar](30) NULL,
	[EmptyWeight] [varchar](30) NULL,
	[MaxTakeoffWeight] [varchar](30) NULL,
	[MinNeededRunwaySize] [varchar](30) NULL,
	[IDSB] [char](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdTt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW11111', N'BOSS001', N'900', N'Thuong', N'500', N'6000', N'30M', NULL)
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW22222', N'BOSS002', N'900', N'Vip', N'500', N'6000', N'40M', NULL)
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW33333', N'BOSS003', N'800', N'Vip', N'500', N'6000', N'30M', NULL)
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW44444', N'BOSS004', N'900', N'Vip', N'500', N'6000', N'50M', NULL)
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW55555', N'BOSS005', N'900', N'Thuong', N'500', N'6000', N'30M', NULL)
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW66666', N'BOSS006', N'900', N'Vip', N'500', N'6000', N'40', NULL)
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW77777', N'BOSS007', N'800', N'Thuong', N'500', N'6000', N'20', NULL)
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW88888', N'BOSS008', N'900', N'Thuong', N'500', N'6000', N'30M', NULL)
INSERT [dbo].[Airplane] ([IdMb], [Model], [CruiseSpeed], [PlaneType], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'FW99999', N'BOSS009', N'700', N'Thuong', N'500', N'6000', N'30M', NULL)
INSERT [dbo].[Airport] ([IdSb], [Name], [Runwaysize], [WingParKingPlace]) VALUES (N'11111  ', N'NOI
BAI', N'NOI
BAI', N'100')
INSERT [dbo].[Airport] ([IdSb], [Name], [Runwaysize], [WingParKingPlace]) VALUES (N'AP11111', N'TAN SAN
NHAT', N'5000M', N'100')
INSERT [dbo].[Airport] ([IdSb], [Name], [Runwaysize], [WingParKingPlace]) VALUES (N'AP22222', N'NOI
BAI', N'6000M', N'100')
INSERT [dbo].[Airport] ([IdSb], [Name], [Runwaysize], [WingParKingPlace]) VALUES (N'AP33333', N'QUOC TE
CAN THO', N'4000M', N'100')
INSERT [dbo].[Airport] ([IdSb], [Name], [Runwaysize], [WingParKingPlace]) VALUES (N'AP44444', N'QUOC TE
DA NANG', N'5000M', N'100')
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW10000', N'KING001', N'1000', N'400', N'3000', N'35M', NULL)
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW20000', N'KING002', N'2000', N'500', N'4000', N'55M', NULL)
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW30000', N'KING003', N'1000', N'400', N'5000', N'35M', NULL)
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW40000', N'KING004', N'1000', N'600', N'7000', N'25M', NULL)
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW50000', N'KING005', N'3000', N'400', N'5000', N'35M', NULL)
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW60000', N'KING006', N'1000', N'400', N'5000', N'40M', NULL)
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW70000', N'KING007', N'1000', N'700', N'8000', N'35M', NULL)
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW80000', N'KING008', N'2000', N'400', N'5000', N'50M', NULL)
INSERT [dbo].[Helicopter] ([IdTt], [Model], [CruiseSpeed], [EmptyWeight], [MaxTakeoffWeight], [MinNeededRunwaySize], [IDSB]) VALUES (N'RW90000', N'KING009', N'1000', N'300', N'4000', N'35M', NULL)
ALTER TABLE [dbo].[Airplane]  WITH CHECK ADD  CONSTRAINT [FK_SANBAY_MAYBAY] FOREIGN KEY([IDSB])
REFERENCES [dbo].[Airport] ([IdSb])
GO
ALTER TABLE [dbo].[Airplane] CHECK CONSTRAINT [FK_SANBAY_MAYBAY]
GO
ALTER TABLE [dbo].[Helicopter]  WITH CHECK ADD  CONSTRAINT [FK_SANBAY_TRUCTHANG] FOREIGN KEY([IDSB])
REFERENCES [dbo].[Airport] ([IdSb])
GO
ALTER TABLE [dbo].[Helicopter] CHECK CONSTRAINT [FK_SANBAY_TRUCTHANG]
GO
